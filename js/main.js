jQuery.exists = function (selector) { // С„СѓРЅРєС†РёСЏ РїСЂРѕРІРµСЂРєРё СЃСѓС‰РµСЃС‚РІРѕРІР°РЅРёСЏ СЃРµР»РµРєС‚РѕСЂР°
    return ($(selector).length > 0);
};




if($.exists("#player")) {

    var tag = document.createElement('script');

    tag.src = "https://www.youtube.com/iframe_api";
    var firstScriptTag = document.getElementsByTagName('script')[0];
    firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);


    var player;
    function onYouTubeIframeAPIReady() {
        player = new YT.Player('player', {
            videoId: 'NmiIgWpRBds',
            playerVars: {
                controls: 0,
                showinfo: 0
            },
            events: {
                'onReady': onPlayerReady,
                'onStateChange': onPlayerStateChange
            }
        });
    }

    function onPlayerReady() {
        $(".video-player").click(function () {
            player.playVideo();
            $(".start-video").hide("slow");
            $(".video-player-bg").removeClass("video-player-bg");
        });
    }

    var done = false;
    function onPlayerStateChange(event) {
        if (event.data == YT.PlayerState.PLAYING && !done) {
            setTimeout(stopVideo, 6000);
            done = true;
        }
    }
    function stopVideo() {
        player.stopVideo();
    }
}



(function() {
    $(document).ready(function() {


        if($.exists(".section6, .questions, .price, .about-us")) {
            window.onload = function() {
                if(!window.location.hash) {
                    window.location = window.location + '#loaded';
                    window.location.reload();
                }
            };
            section6 = $(".section6").height();
            questions = $(".questions").height();
            price = $(".price").height();
            aboutUs = $(".about-us").height();

            $(".section6").css({height: section6 + 70});
            $(".questions").css({height: questions + 150});
            $(".price").css({height: price + 70});
            $(".about-us").css({height: aboutUs + 70});
        }



        if($.exists("#accordion")) {
            function toggleChevron(e) {
                $(e.target)
                    .prev('.panel-heading')
                    .find("i.indicator")
                    .toggleClass('glyphicon-chevron-down glyphicon-chevron-up');
            }
            $('#accordion').on('hidden.bs.collapse', toggleChevron);
            $('#accordion').on('shown.bs.collapse', toggleChevron);
            $('#accordion2').on('hidden.bs.collapse', toggleChevron);
            $('#accordion2').on('shown.bs.collapse', toggleChevron);
        }



        //if($.exists(".selectpicker")) {
        //    $('.selectpicker').selectpicker();
        //}



        if($.exists("#file-upload")) {
            $('#file-upload').change(function() {
                var filepath = this.value;
                var m = filepath.match(/([^\/\\]+)$/);
                var filename = m[1];
                $('#filename').html(filename);

            });
        }



        if($.exists(".select-menu")) {
            var goldTrial = {
                375 : 450,
                500 : 670,
                585 : 999,
                750 : 1277,
                850 : 1450,
                958 : 1549,
                999 : 1693
            };
            var silverTrial = {
                875 : 15,
                925 : 16,
                999 : 23
            };
            var platinumTrial = {
                850 : 1350,
                900 : 1440,
                950 : 1440
            };
            var palladiumTrial = {
                500 : 810,
                850 : 900
            };


            if($.exists(".calc")) {
                $('.ui-slider-handle').draggable({
                    revertDuration: false,
                    scrollSpeed: 1000
                });
                $(".calc").slider({
                    max: 1000,
                    value: 0,
                    slide: function (event, ui) {
                        $('.calc-weight').text(ui.value);

                        var calc = function () {
                            var metalWeight = $('.calc-weight').text();
                            var trialX =  $('.trialX h2').attr('data-trial-val');
                            var result = $('.price-result');
                            var dailyPayment = $('.daily-payment');

                            var calcAll = metalWeight * trialX;
                            var calcDailyPayment = calcAll * 0.05;

                            $(result).html(calcAll.toFixed(2).replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1 ') + " руб.");

                            if($.exists(".daily-payment")){
                                $('.trialX h2, .metalX h2').on('click', function () {
                                    calcDailyPayment = calcAll * 0.05;
                                });
                                $(dailyPayment).html(calcDailyPayment.toFixed(2).replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1 ') + " руб.");
                            }

                            if($(metalWeight) == 0 || $(dailyPayment).html() == 0 || $('.trialX h2').text() == "---" || $('.metalX h2').text() == "---"){
                                $(result).html("не выбрано");
                                $(dailyPayment).html("не выбрано");
                            }else if (trialX == undefined){
                                console.clog("asdasd");
                            }
                        };

                        calc();


                        var recalc = function () {
                            var metalWeight2 = $('.calc-weight').text();
                            var trialX2 =  $('.trialX h2').attr('data-trial-val');
                            var result2 = $('.price-result');
                            var dailyPayment2 = $('.daily-payment');
                            var calcAll2 = metalWeight2 * trialX2;
                            var calcDailyPayment2 = calcAll2 * 0.05;

                            $(result2).html(calcAll2.toFixed(2).replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1 ') + " руб.");

                            if($.exists(".daily-payment")){
                                $('.daily-payment').html(calcDailyPayment2.toFixed(2).replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1 ') + " руб.");
                            }

                            if($(metalWeight2) == 0 || $(dailyPayment2).html() == 0 || $('.trialX h2').text() == "---" || $('.metalX h2').text() == "---"){
                                $(result2).html("не выбрано");
                                $(dailyPayment2).html("не выбрано");
                            }else if (trialX2 == undefined){
                                console.clog("asdasd");
                            }
                        };


                        $('.select-menu').on('click', 'li', function () {
                            recalc();
                        });

                        //if($(result).html() == 'не выбрано' || $(dailyPayment).html() == "не выбрано"){
                        //    $('.trialX h2, .metalX h2').on('click', function () {
                        //        var metalWeight2 = $('.calc-weight').text();
                        //        var trialX2 =  $('.trialX h2').attr('data-trial-val');
                        //
                        //        var newСalcAll = metalWeighе2 * trialX2;
                        //        var newСalcDailyPayment = newСalcAll * 0.05;
                        //        $(result).html(newСalcAll.toFixed(2) + " руб.");
                        //        $(dailyPayment).html(newСalcDailyPayment.toFixed(2) + " руб.");
                        //    });
                        //}
                    }
                }).slider("pips", {
                    first: "pip",
                    last: "pip",
                    rest: false
                });
            }



            $('.select-menu').on('click', 'h2', function () {
                $(this).parent().toggleClass('open');
                $(this).parent().find('ul').toggle();

                if($(this).parent().hasClass('metalX')){
                    $('.trialX h2').html("---");
                    $('.trialX h2').removeAttr('data-trial-val');
                }
            });


            $('.select-menu ul').on('click', 'li', function () {
                var selectItem = $(this).html();
                var selectVal = $(this).data('trial-val');

                if($(this).parent().parent().hasClass('metalX')){
                    $('.trialX h2').html("---");
                    $('.trialX h2').removeAttr('data-trial-val');
                }

                $(this).parent().parent().find('h2').html(selectItem);
                $(this).parent().parent().find('h2').attr('data-trial-val', selectVal);
                $(this).parent().toggle();
                $(this).parent().parent().toggleClass('open');
            });


            $('.metalX ul').on('click', 'li', function(e) {
                var selectedMetal = $(e.target).html();

                var allMetal = [];
                $('.metalX ul li').each(function () {
                    allMetal.push($(this).text());
                });

                if(selectedMetal == allMetal[0]){
                    $('.trialX ul').find('li').remove();
                    $.each( goldTrial, function(key, val){
                        $('.trialX ul').append("<li data-trial-val=" + val + ">" +key+ "</li>");
                    });
                }
                else if(selectedMetal == allMetal[1]){
                    $('.trialX ul').find('li').remove();
                    $.each( silverTrial, function(key, val){
                        $('.trialX ul').append("<li data-trial-val=" + val + ">" +key+ "</li>");
                    });
                }
                else if(selectedMetal == allMetal[2]){
                    $('.trialX ul').find('li').remove();
                    $.each( platinumTrial, function(key, val){
                        $('.trialX ul').append("<li data-trial-val=" + val + ">" +key+ "</li>");
                    });
                }
                else if(selectedMetal == allMetal[3]){
                    $('.trialX ul').find('li').remove();
                    $.each( palladiumTrial, function(key, val){
                        $('.trialX ul').append("<li data-trial-val=" + val + ">" +key+ "</li>");
                    });
                }

            });

        }




        if($.exists("#container")) {
            $('#container input[type=range]').each(function(i, el) {
                var stylesheet = $(document.createElement('style')).appendTo(document.body);
                $(el).
                    on('input', function() {
                        stylesheet[0].textContent = 'input[type=\'range\']::-webkit-slider-thumb:before { content: "' +
                        $(this).val() + '"; }';
                    }).
                    trigger('input');
            });
        }






        if($.exists(".open-pop-up")) {
            $('.open-pop-up:not(.isnt)').click(function (e) {
                e.preventDefault();
                var href = $(this).attr('href');


                if ($(href).hasClass('active-pop-up')) {
                    $('.pop-up-wrap').removeClass('active-pop-up');
                    $(href).removeClass('active-pop-up');
                    $('#parallax div').addClass('parallax');

                }else{
                    $('html,body').addClass('overflowed');
                    $('.pop-up-wrap').removeClass('active-pop-up');
                    $(href).addClass('active-pop-up');
                    $('#parallax div').removeClass('parallax');
                }


            });
        }

        $('.close-pop-up').click(function(){
            $('html,body').removeClass('overflowed');
        });

        $('.open-pop-up.isnt').click(function(){
            location.href = $(this).attr('href');
        });




        if($.exists(".parallax")) {
            //Illustration by http://psdblast.com/flat-color-abstract-city-background-psd
            $(window).on('mousemove', function(e) {
                var w = $(window).width();
                var h = $(window).height();
                var offsetX = 0.5 - e.pageX / w;
                var offsetY = 0.5 - e.pageY / h;

                $(".parallax").each(function(i, el) {
                    var offset = parseInt($(el).data('offset'));
                    var translate = "translate3d(" + Math.round(offsetX * offset) + "px," + Math.round(offsetY * offset) + "px, 0px)";

                    $(el).css({
                        '-webkit-transform': translate,
                        'transform': translate,
                        'moz-transform': translate
                    });
                });
            });
        }






        if($.exists("#sidr-menu")) {
            $('#sidr-menu').sidr({
                speed: 0
            });
        }







        if($.exists(".menu-btn")) {
            $(".menu-btn").click(function(){
                $(this).toggleClass('open');
                $("body").toggleClass('open');
            });
        }



        if($.exists(".arr-bot")) {
            $('.arr-bot').click(function(){
                if(document.getElementById($(this).attr('href').substr(1)) != null) {
                    $('html, body').animate({ scrollTop: $($(this).attr('href')).offset().top }, 500);
                }
                return false;
            });
        }




        if($.exists("#slide-checkbox")) {
            $('#slide-checkbox').click(function (e) {
                $('.from-to').toggleClass('not-active');
                $('.slide-checkbox').toggleClass('active');
            });
        }




        if($.exists(".how-it-work")) {
            $('.how-it-work-item').click(function (e) {
                var item = $(this);
                var itemText = $('.section3-content');
                $('.how-it-work-item').removeClass('active');
                $(itemText).removeClass('active');

                if(item.hasClass('active')){
                    $(item).removeClass('active');
                    $(itemText).eq(item.index()).removeClass('active');
                } else {
                    $(item).addClass('active');
                    $(itemText).eq(item.index() -2).addClass('active');
                }


            });
        }




        if($.exists("#calculator-box")) {
            $('#calculator-box .form1 .submit1').click(function (e) {

                // var status = true;
                // $('.metalX, .productX, .trialX').each(function(i, input){
                //     if ($(input).find('h2').text().toString().trim() == '---'){
                //         status = false;
                //     }
                // });

                // if (parseInt($('.calc-weight').text()) < 1){
                //     status = false;
                // }
                // if ($('.calc-textarea').val().toString().trim().length < 10){
                //     status = false;
                // }


                // if (status == true){
                //     $('.form1').slideUp("slow");
                //     $('.form2').slideDown("slow");
                // }else{
                //     $('#alert-popup').addClass('active-pop-up');
                // }
                $('.form1').slideUp("slow");
                $('.form2').slideDown("slow");
                $('html, body').animate({ scrollTop: $('#calculator-box').offset().top }, 500);

                e.preventDefault();
            });
        }

        $('#sendFormData').click(function(e){
            var status = true;
            $('.form2 #input1, .form2 #input2').each(function(i, input){
                if ($(input).val().toString().trim().length < 3){
                    status = false;
                }
            });

            if (status == false){
                e.preventDefault();
                $('#alert-popup2').addClass('active-pop-up');
            }
        });



        if($.exists(".header-ul")) {

            $('.header-ul li a:not(:hover)').each(function() {
                $(this).hover(function(){
                    $('.header-ul li a:not(:hover)').css({opacity : 0.5});
                }, function(){
                    $('.header-ul li a:not(:hover)').css({opacity : 1});
                });
            })

        }


        $('[data-nolink="true"]').on('click tap mousedown mouseup', function(e){
            e.preventDefault();
        });

        //if($.exists(".calc-all")) {
        //
        //    $('.pick-material').change(function () {
        //        var material = $('.pick-material .dropdown-menu').children('li.selected').attr('data-original-index');
        //        var goldTrial = [375,500,585,750,850,958,999];
        //        var silverTrial = [875,925,999];
        //        var platinumTrial = [850,900,950];
        //        var palladiumTrial = [500,850];
        //
        //        console.log(material);
        //
        //        if(material == '0'){
        //            $.each( goldTrial, function(i, str){
        //                $('.pick-trial .dropdown-menu').find("[data-original-index=" + i + "]").find('.text').text(str);
        //            });
        //        }
        //        else if(material == '1'){
        //            $.each( silverTrial, function(i, str){
        //                $('.pick-trial .dropdown-menu').find("[data-original-index=" + i + "]").find('.text').text(str);
        //            });
        //        }
        //        else if(material == '2'){
        //            $.each( platinumTrial, function(i, str){
        //                $('.pick-trial .dropdown-menu').find("[data-original-index=" + i + "]").find('.text').text(str);
        //            });
        //        }
        //        else if(material == '3'){
        //            $.each( palladiumTrial, function(i, str){
        //                $('.pick-trial .dropdown-menu').find("[data-original-index=" + i + "]").find('.text').text(str);
        //            });
        //        }
        //
        //
        //    });
        //
        //}


    });
}).call(this);